﻿#include <iostream>
#include <string>
#include <set>


struct RankInADynamicSubset {
	inline bool Add(const std::string& entry) noexcept {
		if (entry.length() != 2) {
			return false;
		}
		switch (entry[0]) {
		case 'H': case 'D': case 'C': case 'S': break;
		default: return false;
		}
		switch (entry[1]) {
		case 'A': case '2': case '3': case '4': case '5':
		case '6': case '7': case '8': case '9': case 'T':
		case 'J': case 'Q': case 'K': break;
		default: return false;
		}
		if (this->list.find(entry) != this->list.end()) {
			return false;
		}
		this->list.insert(entry);
		return true;
	}

private:
	std::set<std::string> list;
};


int main(const int argc, const char *const *const argv)
{
	RankInADynamicSubset rankInADynamicSubset;
	bool result = true;
	int n;

	std::cin >> n;
	for (int i = 0; i < n; ++i) {
		std::string s;
		std::cin >> s;
		result &= rankInADynamicSubset.Add(s);
	}
	if (result) {
		std::cout << "Yes";
	}
	else {
		std::cout << "No";
	}

	return 0;
}
