﻿#include <iostream>
#include <algorithm>


static int solve(const int x, const int y, const int b, const int p) {
	const auto bplus = std::max(5, b);
	const auto pplus = std::max(2, p);
	return std::min(x * b + y * p, (x * bplus + y * pplus) * 8 / 10);
}


int main() {
	int n;
	std::cin >> n;
	for (int i = 0; i < n; ++i) {
		int x, y, b, p;
		std::cin >> x >> y >> b >> p;
		std::cout << solve(x, y, b, p) << std::endl;
	}

	return 0;
}
