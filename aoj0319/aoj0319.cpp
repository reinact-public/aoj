﻿#include <algorithm>
#include <iostream>
#include <vector>


namespace {
	template<typename T>
	inline T GetNext(std::istream& istream) {
		T value;
		istream >> value;
		return value;
	}

	template<typename T>
	inline std::istream& operator>>(std::istream& istream, std::vector<T>& vector) {
		for (T& item : vector) {
			istream >> item;
		}
		return istream;
	}
}


int main()
{
	std::vector<size_t> d(GetNext<size_t>(std::cin));
	std::cin >> d;
	std::sort(d.begin(), d.end(), std::greater<size_t>());
	auto hIndex = size_t(0);
	for (; hIndex < d.size(); ++hIndex) {
		if (d[hIndex] < hIndex + 1) {
			break;
		}
	}
	std::cout << hIndex << std::endl;

	return 0;
}
