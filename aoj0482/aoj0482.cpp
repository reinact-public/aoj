﻿#include <iostream>
#include <algorithm>


int main(const int argc, const char *const *const argv)
{
	size_t n;
	std::cin >> n;
	auto r = n;
	for (size_t i = 0; i < n; ++i) {
		int c;
		std::cin >> c;
		r -= !!(c == 1);
	}
	std::cout << std::max(static_cast<size_t>(1), r) << std::endl;

	return 0;
}
