﻿#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>


namespace {
	template<typename T>
	struct RankInADynamicSubset {
	public:
		template <class InputIterator>
		RankInADynamicSubset(const InputIterator begin, const InputIterator end);

		inline void Add(T object)
		{
			this->list.push_back(object);
		}

		inline void Sort()
		{
			std::sort(this->list.begin(), this->list.end());
		}

		inline int Sum()
		{
			return std::accumulate(this->list.begin(), this->list.end(), 0);
		}

		inline const T& operator[](int index) const {
			while (index < 0) {
				index += this->list.size();
			}
			return this->list[index % this->list.size()];
		}

	private:
		std::vector<T> list;
	};
}


int main(const int argc, const char *const *const argv)
{
	RankInADynamicSubset<int> d;
	size_t n;
	int m;
	std::cin >> n >> m;
	for (int i = 0; i < n; ++i) {
		int t;
		std::cin >> t;
		d.Add(t);
	}
	d.Sort();

	int64_t result = 0;
	for (int i = 0; i < n;) {
		int64_t sum = d[i];
		int j = i + 1;
		for (; j % n != i && (d[j - 1] == d[j] || (d[j - 1] + 1) % m == d[j]); ++j) {
			sum += d[j];
		}
		i = j;
		result = std::max(result, sum);
	}

	std::cout << (d.Sum() - result) << std::endl;

	return 0;
}
