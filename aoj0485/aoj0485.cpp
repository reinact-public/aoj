﻿#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

int main(const int argc, const char *const *const argv)
{
	size_t n, m;
	std::cin >> n >> m;
	std::vector<int> houses(n, 0);
	for (int i = 0; i < m; ++i) {
		int t;
		size_t h;
		std::cin >> t >> h;
		houses[h - 1] = t;
	}

	for (size_t src = 0, dst = 0; src < n; ++src) {
		if (houses[src] <= 0) {
			continue;
		}
		for (dst = std::max(dst, src); dst < n && houses[dst] > 0; ++dst) { }
		if (dst < n) {
			houses[dst] = houses[src];
		}
	}

	std::cout << std::accumulate(houses.cbegin(), houses.cend(), 0) << std::endl;

	return 0;
}
