﻿#include <iostream>
#include <vector>


int main()
{
	size_t n, m;
	std::cin >> n >> m;
	std::vector<std::vector<std::pair<size_t, int>>> records(n);
	for (; ; ) {
		size_t s, t;
		int e;
		std::cin >> s >> t >> e;
		if (s == 0 && t == 0 && e == 0) {
			break;
		}
		records[s - 1].emplace_back(t - 1, e);
	}

	size_t l;
	std::cin >> l;
	for (auto i = size_t(0); i < l; ++i) {
		std::vector<int> b(m);
		for (auto j = size_t(0); j < m; ++j) {
			std::cin >> b[j];
		}

		for (auto j = size_t(0); j < n; ++j) {
			auto c = 0;
			for (const auto& a : records[j]) {
				c += a.second * b[a.first];
			}
			if (j > 0) {
				std::cout << " ";
			}
			std::cout << c;
		}
		std::cout << std::endl;
	}

	return 0;
}
