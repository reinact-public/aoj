﻿#include <iostream>
#include <vector>
#include <queue>


struct SearchStatus {
private:
	size_t level;
	size_t x;
	size_t y;
	size_t count;
public:
	constexpr SearchStatus(size_t level, size_t x, size_t y, size_t count) noexcept :
		level(level), x(x), y(y), count(count) { }
	constexpr decltype(auto) GetLevel() const noexcept { return this->level; }
	constexpr decltype(auto) GetX() const noexcept { return this->x; }
	constexpr decltype(auto) GetY() const noexcept { return this->y; }
	constexpr decltype(auto) GetCount() const noexcept { return this->count; }
};


int main(const int argc, const char *const *const argv)
{
	size_t h, w;
	std::cin >> h >> w;
	std::vector<std::string> map;
	for (int i = 0; i < h; ++i) {
		std::string s;
		std::cin >> s;
		map.push_back(s);
	}

	std::vector<std::vector<std::vector<int>>> d(2,
		std::vector<std::vector<int>>(h, std::vector<int>(w, -1)));
	std::queue<SearchStatus> queue;
	for (int y = 0; y < h; ++y) {
		for (int x = 0; x < w; ++x) {
			if (map[y][x] == 'S') {
				queue.push(SearchStatus(0, x, y, 0));
			}
		}
	}
	while (!queue.empty()) {
		const SearchStatus s = queue.front();
		queue.pop();
		if (s.GetX() < 0 || s.GetX() >= w || s.GetY() < 0 || s.GetY() >= h || s.GetLevel() >= 2) {
			continue;
		}
		if (d[s.GetLevel()][s.GetY()][s.GetX()] >= 0) {
			continue;
		}
		d[s.GetLevel()][s.GetY()][s.GetX()] = s.GetCount();
		switch (map[s.GetY()][s.GetX()]) {
		case 'G':
			std::cout << s.GetCount() << std::endl;
			goto END;
		case '.':
		case 'S':
			queue.push(SearchStatus(s.GetLevel(), s.GetX() - 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel(), s.GetX() + 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel(), s.GetX(), s.GetY() - 1, s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel(), s.GetX(), s.GetY() + 1, s.GetCount() + 1));
			break;
		case 'U':
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX() - 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX() + 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel(), s.GetX(), s.GetY() - 1, s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX(), s.GetY() + 1, s.GetCount() + 1));
			break;
		case 'D':
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX() - 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX() + 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX(), s.GetY() - 1, s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel(), s.GetX(), s.GetY() + 1, s.GetCount() + 1));
			break;
		case 'L':
			queue.push(SearchStatus(s.GetLevel(), s.GetX() - 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX() + 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX(), s.GetY() - 1, s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX(), s.GetY() + 1, s.GetCount() + 1));
			break;
		case 'R':
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX() - 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel(), s.GetX() + 1, s.GetY(), s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX(), s.GetY() - 1, s.GetCount() + 1));
			queue.push(SearchStatus(s.GetLevel() + 1, s.GetX(), s.GetY() + 1, s.GetCount() + 1));
			break;
		default:
			break;
		}
	}
	std::cout << "-1" << std::endl;

END:
	return 0;
}
