﻿#include <iostream>
#include <array>
#include <algorithm>


static int Solve(int c, int a, int n) {
    // Team: C, A, N
    const int num_can = std::min({ c, a, n });
    c -= num_can;
    a -= num_can;
    n -= num_can;

    // Team: C, C, A
    const int num_cca = std::min(c / 2, a);
    c -= num_cca * 2;
    a -= num_cca;

    // Team: C, C, C
    const int num_ccc = c / 3;
    c -= num_ccc * 3;

    return num_can + num_cca + num_ccc;
}


int main() {
    int q;
    std::cin >> q;
    for (int i = 0; i < q; ++i) {
        int c, a, n;
        std::cin >> c >> a >> n;
        std::cout << Solve(c, a, n) << std::endl;
    }
    return 0;
}
