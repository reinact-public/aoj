﻿#include <iostream>


int main()
{
	size_t n;
	std::cin >> n;
	for (auto i = size_t(0); i < n; ++i) {
		int r, t;
		std::cin >> r >> t;

		if (r % 100 == 0 && t % 30 == 0) {
			std::cout << ((t / 30) * 5 + r / 100) << std::endl;
		}
		else if (r % 100 == 0) {
			std::cout << ((t / 30) * 5 + r / 100) << " "
				<< ((t / 30 + 1) * 5 + r / 100) << std::endl;
		}
		else if (t % 30 == 0) {
			std::cout << ((t / 30) * 5 + r / 100) << " "
				<< ((t / 30) * 5 + r / 100 + 1) << std::endl;
		}
		else {
			std::cout << ((t / 30) * 5 + r / 100) << " "
				<< ((t / 30) * 5 + r / 100 + 1) << " "
				<< ((t / 30 + 1) * 5 + r / 100) << " "
				<< ((t / 30 + 1) * 5 + r / 100 + 1) << std::endl;
		}
	}

	return 0;
}
