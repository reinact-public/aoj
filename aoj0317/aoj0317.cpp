﻿#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>


namespace {
    inline size_t GetNext(std::istream& istream) {
        size_t value;
        istream >> value;
        return value;
    }
}


int main()
{
    const auto d = GetNext(std::cin);
    const auto l = GetNext(std::cin);
    std::vector<size_t> opt(d + 1);
    std::iota(opt.begin(), opt.begin() + std::min(d + 1, l), 0);
    for (auto i = l; i < d + 1; ++i) {
        opt[i] = std::min(opt[i - l], opt[i - 1]) + 1;
    }
    std::cout << opt[d] << std::endl;

    return 0;
}
