﻿#include <iostream>
#include <algorithm>

constexpr int JMAT[3][3] = {
	{0, 1, -1},
	{-1, 0, 1},
	{1, -1, 0}
};


int main(int argc, const char *const *const argv)
{
	auto total1 = 0;
	auto total2 = 0;
	for (int i = 0; i < 3; ++i) {
		int x, y;
		std::cin >> x >> y;
		total1 += std::max(0, JMAT[x][y]);
		total2 += std::max(0, -JMAT[x][y]);
	}
	if (total2 > total1) {
		std::cout << "1" << std::endl;
	}
	else if (total1 > total2) {
		std::cout << "0" << std::endl;
	}
	else {
		std::cout << "-1" << std::endl;
	}

	return 0;
}
