﻿#include <iostream>
#include <string>
#include <vector>
#include <algorithm>


static std::vector<int> solve(const int n, const std::string& s) {
	std::vector<int> d(n, 0);
	int field = 0;
	for (int i = 0; i < s.size(); ++i) {
		switch (s[i]) {
		case 'M':
			d[i % n] += 1;
			break;
		case 'S':
			field += d[i % n] + 1;
			d[i % n] = 0;
			break;
		case 'L':
			d[i % n] += field + 1;
			field = 0;
			break;
		default:
			throw std::invalid_argument("Fatal error.");
		}
	}
	std::sort(d.begin(), d.end());
	d.push_back(field);
	return d;
}


int main() {
	for (;;) {
		int n;
		std::cin >> n;
		if (n == 0) {
			break;
		}
		std::string s;
		std::cin >> s;
		const auto result = solve(n, s);
		for (auto p = result.cbegin(); p != result.cend(); ++p) {
			if (p != result.begin()) {
				std::cout << " ";
			}
			std::cout << *p;
		}
		std::cout << std::endl;
	}

	return 0;
}
