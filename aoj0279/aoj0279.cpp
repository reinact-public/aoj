﻿#include <iostream>
#include <vector>
#include <algorithm>


int main() {
	for (;;) {
		int n;
		std::cin >> n;
		if (n == 0) {
			break;
		}
		bool possible = false;
		int count = 0;
		for (int i = 0; i < n; ++i) {
			int t;
			std::cin >> t;
			possible |= !!(t >= 2);
			count += !!(t > 0);
		}
		if (possible) {
			std::cout << (count + 1) << std::endl;
		}
		else {
			std::cout << "NA" << std::endl;
		}
	}
	return 0;
}
