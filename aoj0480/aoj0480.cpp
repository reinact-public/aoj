﻿#include <iostream>
#include <algorithm>


inline decltype(auto) GetData(std::istream& istream) {
	int first, second;
	istream >> first >> second;
	return first * 10 + second;
}


int main(const int argc, const char *const *const argv)
{
	const auto value = GetData(std::cin);
	const auto average = GetData(std::cin);

	std::cout << (!!(value >= 375) + !!(value >= average)) << std::endl;

	return 0;
}
