﻿#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <vector>


namespace {
	template<typename T>
	inline T GetNext(std::istream& istream) {
		T value;
		istream >> value;
		return value;
	}

	struct MatrixBuffer2D {
	private:
		size_t rows;
		size_t cols;
		std::vector<bool> data;
	public:
		inline MatrixBuffer2D(size_t rows, size_t cols)
			: rows(rows)
			, cols(cols)
			, data(rows * cols, false) { }
		inline bool At(size_t i, size_t j) const {
			return this->data[i * this->cols + j];
		}
		inline bool Set(size_t i, size_t j, bool value) {
			this->data[i * this->cols + j] = value;
			return value;
		}
		inline std::set<std::pair<size_t, size_t>> GetHorizontalAsymmetries() const {
			std::set<std::pair<size_t, size_t>> asymmetries;
			for (auto i = size_t(0); i < this->rows; ++i) {
				for (auto j = size_t(0), k = this->cols - 1; j < k; ++j, --k) {
					if (this->At(i, j) != this->At(i, k)) {
						asymmetries.emplace(i, j);
					}
				}
			}
			return asymmetries;
		}
		inline std::set<std::pair<size_t, size_t>> GetVerticalAsymmetries() const {
			std::set<std::pair<size_t, size_t>> asymmetries;
			for (auto i = size_t(0), j = this->rows - 1; i < j; ++i, --j) {
				for (auto k = size_t(0); k < this->cols; ++k) {
					if (this->At(i, k) != this->At(j, k)) {
						asymmetries.emplace(i, k);
					}
				}
			}
			return asymmetries;
		}
	};
}


int main()
{
	const auto c = GetNext<size_t>(std::cin);
	const auto n = GetNext<size_t>(std::cin);
	MatrixBuffer2D buffer(n, n);
	for (auto i = size_t(0); i < n; ++i) {
		const auto s = GetNext<std::string>(std::cin);
		for (auto j = size_t(0); j < n; ++j) {
			buffer.Set(i, j, s[j] != '0');
		}
	}

	auto h = buffer.GetHorizontalAsymmetries();
	auto v = buffer.GetVerticalAsymmetries();
	auto count = static_cast<size_t>(!!(h.empty() && v.empty()));
	for (auto i = size_t(1); i < c; ++i) {
		const auto d = GetNext<size_t>(std::cin);
		for (auto j = size_t(0); j < d; ++j) {
			const auto row = GetNext<size_t>(std::cin) - 1;
			const auto col = GetNext<size_t>(std::cin) - 1;
			const auto hpair = std::make_pair(row, std::min(col, n - col - 1));
			const auto hiter = h.find(hpair);
			if (hiter == h.end()) {
				h.insert(hpair);
			}
			else {
				h.erase(hiter);
			}
			const auto vpair = std::make_pair(std::min(row, n - row - 1), col);
			const auto viter = v.find(vpair);
			if (viter == v.end()) {
				v.insert(vpair);
			}
			else {
				v.erase(vpair);
			}
		}
		count += !!(h.empty() && v.empty());
	}
	std::cout << count << std::endl;

	return 0;
}
