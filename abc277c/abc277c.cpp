﻿#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <map>


namespace {
    struct RankInADynamicSubset {
    public:
        inline void Connect(const int i, const int j) {
            this->list.emplace(i, j);
            this->list.emplace(j, i);
        }

        inline int Get(const int start) const noexcept {
            int result = start;
            std::set<int> visited;
            std::queue<int> queue;
            queue.push(start);
            while (!queue.empty()) {
                const auto index = queue.front();
                queue.pop();
                if (visited.find(index) != visited.end()) {
                    continue;
                }
                visited.insert(index);
                result = std::max(result, index);
                const auto range = this->list.equal_range(index);
                for (auto it = range.first; it != range.second; ++it) {
                    queue.push(it->second);
                }
            }

            return result;
        }

    private:
        std::multimap<int, int> list;

    };
}


int main(const int argc, const char *const *const argv)
{
    RankInADynamicSubset rankInADynamicSubset;

    int n;
    std::cin >> n;
    for (int i = 0; i < n; ++i) {
        int a, b;
        std::cin >> a >> b;
        rankInADynamicSubset.Connect(a, b);
    }
    std::cout << rankInADynamicSubset.Get(1);

    return 0;
}
