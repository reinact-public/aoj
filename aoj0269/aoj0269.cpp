﻿#define _USE_MATH_DEFINES
#include <algorithm>
#include <cmath>
#include <complex>
#include <iostream>
#include <vector>


namespace {
	enum struct TreeKind {
		PLUM,
		PLUM_OTHER,
		PEACH,
		CHERRY
	};

	struct Wind {
	private:
		double angle;
		double radius;
	public:
		constexpr Wind(double angle, double radius)
			: angle(angle)
			, radius(radius) { }
		constexpr double GetAngle() const { return this->angle; }
		constexpr double GetRadius() const { return this->radius; }
	};

	struct Tree {
	private:
		TreeKind kind;
		std::complex<double> pos;
		double angle;
	public:
		constexpr Tree(TreeKind kind, double x, double y, double angle)
			: kind(kind)
			, pos(x, y)
			, angle(angle) { }
		constexpr TreeKind GetKind() const { return this->kind; }
		constexpr std::complex<double> GetPos() const { return this->pos; }
		constexpr double GetAngle() const { return this->angle; }

		inline bool Contains(const Wind& wind, const std::complex<double>& pos) const {
			const auto distance = pos - this->pos;
			if (std::abs(distance) > wind.GetRadius()) {
				return false;
			}

			const auto argument = std::arg(distance) * 180 / M_PI;
			const auto angdiff = std::abs((argument < 0 ? 360 + argument : argument) - wind.GetAngle());
			return std::min(angdiff, 360 - angdiff) <= this->angle / 2;
		}
	};
}


int main()
{
	for (; ; ) {
		std::vector<std::complex<double>> houses;
		std::vector<Tree> trees;
		std::vector<Wind> winds;
		{
			size_t h, r;
			std::cin >> h >> r;
			if (h == 0 && r == 0) {
				break;
			}
			for (auto i = size_t(0); i < h; ++i) {
				double x, y;
				std::cin >> x >> y;
				houses.emplace_back(x, y);
			}
			size_t u, m, s;
			std::cin >> u >> m >> s;
			double du, dm, ds;
			std::cin >> du >> dm >> ds;
			trees.emplace_back(TreeKind::PLUM, 0, 0, du);
			for (auto i = size_t(0); i < u; ++i) {
				double x, y;
				std::cin >> x >> y;
				trees.emplace_back(TreeKind::PLUM_OTHER, x, y, du);
			}
			for (auto i = size_t(0); i < m; ++i) {
				double x, y;
				std::cin >> x >> y;
				trees.emplace_back(TreeKind::PEACH, x, y, dm);
			}
			for (auto i = size_t(0); i < s; ++i) {
				double x, y;
				std::cin >> x >> y;
				trees.emplace_back(TreeKind::CHERRY, x, y, ds);
			}
			for (auto i = size_t(0); i < r; ++i) {
				double w, a;
				std::cin >> w >> a;
				winds.emplace_back(w, a);
			}
		}

		std::vector<size_t> days(houses.size(), 0);
		for (const auto& wind : winds) {
			for (auto i = size_t(0); i < houses.size(); ++i) {
				const auto& house = houses[i];
				auto available = false;
				auto contaminated = false;
				for (const auto& tree : trees) {
					if (tree.Contains(wind, house)) {
						if (tree.GetKind() == TreeKind::PLUM) {
							available = true;
						}
						else {
							contaminated = true;
						}
					}
				}
				if (available && !contaminated) {
					days[i] += 1;
				}
			}
		}

		const auto maxDays = *std::max_element(days.cbegin(), days.cend());
		if (maxDays <= 0) {
			std::cout << "NA" << std::endl;
		}
		else {
			auto rest = false;
			for (auto i = size_t(0); i < houses.size(); ++i) {
				if (days[i] >= maxDays) {
					if (rest) {
						std::cout << " ";
					}
					rest = true;
					std::cout << (i + 1);
				}
			}
			std::cout << std::endl;
		}
	}

	return 0;
}
