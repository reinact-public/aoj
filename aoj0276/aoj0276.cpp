﻿#include <iostream>


constexpr int solve(const int a, const int b) {
	return a - b;
}


int main() {
	for (int i = 0; i < 7; ++i) {
		int a, b;
		std::cin >> a >> b;
		std::cout << solve(a, b) << std::endl;
	}

	return 0;
}
