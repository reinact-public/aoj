﻿#include <climits>
#include <iostream>
#include <mutex>


struct RankInADynamicSubset {
	RankInADynamicSubset() noexcept :
		min(INT_MAX), max(INT_MIN) { }

	void Add(int value) noexcept
	{
		std::unique_lock<decltype(this->mutex)> lock(this->mutex);
		this->min = std::min(this->min, value);
		this->max = std::max(this->max, value);
	}

	decltype(auto) Contains(int value) const noexcept
	{
		std::unique_lock<decltype(this->mutex)> lock(this->mutex);
		return this->min <= value && value <= this->max;
	}

private:
	mutable std::mutex mutex;
	int min;
	int max;
};


int main(const int argc, const char *const *const argv)
{
	RankInADynamicSubset subsets[3];
	subsets[0].Add(0);
	subsets[2].Add(INT_MAX);

	int n;
	std::cin >> n;
	for (int i = 0; i < n; ++i) {
		int a, s;
		std::cin >> a >> s;
		subsets[s].Add(a);
	}

	int m;
	std::cin >> m;
	for (int i = 0; i < m; ++i) {
		int b;
		std::cin >> b;
		if (subsets[0].Contains(b)) {
			std::cout << "0" << std::endl;
		}
		else if (subsets[1].Contains(b)) {
			std::cout << "1" << std::endl;
		}
		else if (subsets[2].Contains(b)) {
			std::cout << "2" << std::endl;
		}
		else {
			std::cout << "-1" << std::endl;
		}
	}

	return 0;
}
