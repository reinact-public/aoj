﻿#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <string>


int main()
{
	size_t j, y;
	std::cin >> j >> y;

	std::string s;
	for (auto i = size_t(0); i < j; ++i) {
		s += "A";
	}
	for (auto i = size_t(0); i < y; ++i) {
		s += "B";
	}

	do {
		auto pa = size_t(0);
		auto pb = size_t(0);
		auto skip = false;
		for (auto it = s.cbegin(); it + 1 != s.cend(); ++it) {
			switch (*it) {
			case 'A': pa += 1; break;
			case 'B': pb += 1; break;
			default: throw std::range_error("Unknown character has been detected.");
			}
			if (std::min(pa, pb) <= 3 && std::max(pa, pb) >= 5) {
				skip = true;
				break;
			}
			if (std::min(pa, pb) >= 4 && pa + pb >= 10) {
				skip = true;
				break;
			}
		}
		if (!skip) {
			std::cout << s << std::endl;
		}
	} while (std::next_permutation(s.begin(), s.end()));

	return 0;
}
