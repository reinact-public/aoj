﻿#include <iostream>


int main(const int argc, const char *const *const argv)
{
	int n, x;
	std::cin >> n >> x;
	for (int i = 0; i < n; ++i) {
		int d;
		std::cin >> d;
		if (d == x) {
			std::cout << (i + 1) << std::endl;
		}
	}

	return 0;
}
