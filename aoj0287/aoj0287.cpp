﻿#include <algorithm>
#include <array>
#include <iostream>
#include <string>


namespace {
    const std::array<const char*, 18> BigIntegerSuffixes{
        "", "Man", "Oku", "Cho", "Kei", "Gai", "Jo", "Jou", "Ko", "Kan",
        "Sei", "Sai", "Gok", "Ggs", "Asg", "Nyt", "Fks", "Mts" };

    struct BigInteger {
    private:
        std::array<int, 18> digits;
    public:
        inline BigInteger()
            : digits() { }
        inline BigInteger(unsigned int value) {
            for (auto it = digits.begin(); it != digits.end(); ++it) {
                *it = value % 10000;
                value /= 10000;
            }
        }
        inline BigInteger(std::array<int, 18> digits)
            : digits(digits) {
            for (auto i = size_t(1); i < this->digits.size(); ++i) {
                this->digits[i] += this->digits[i - 1] / 10000;
                this->digits[i - 1] %= 10000;
            }
        }
        inline BigInteger& operator+=(const BigInteger& o) {
            for (auto i = size_t(0); i < this->digits.size(); ++i) {
                this->digits[i] += o.digits[i];
                if (i > 0) {
                    this->digits[i] += this->digits[i - 1] / 10000;
                    this->digits[i - 1] %= 10000;
                }
            }
            return *this;
        }
        inline BigInteger& operator*=(const BigInteger& o) {
            const auto d1 = this->digits;
            const auto d2 = o.digits;
            std::fill(this->digits.begin(), this->digits.end(), 0);
            for (auto i = size_t(0); i < digits.size(); ++i) {
                decltype(this->digits) part{};
                for (auto j = size_t(i); j < part.size(); ++j) {
                    part[j] = d2[j - i] * d1[i];
                }
                *this += BigInteger(part);
            }
            return *this;
        }
        inline friend std::ostream& operator<<(std::ostream& os, const BigInteger& value) {
            auto written = false;
            for (auto i = size_t(0); i < value.digits.size(); ++i) {
                const auto index = value.digits.size() - 1 - i;
                if (value.digits[index] > 0) {
                    os << value.digits[index] << BigIntegerSuffixes[index];
                    written = true;
                }
            }
            if (!written) {
                os << "0";
            }
            return os;
        }
    };
}

int main()
{
    for (; ; ) {
        int m, n;
        std::cin >> m >> n;
        if (m == 0 && n == 0) {
            break;
        }

        BigInteger result(1);
        BigInteger base(m);
        for (auto p = n; p != 0; p /= 2) {
            if (p % 2 == 1) {
                result *= base;
            }
            base *= base;
        }
        std::cout << result << std::endl;
    }

    return 0;
}
