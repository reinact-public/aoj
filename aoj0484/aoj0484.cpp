﻿#include <iostream>
#include <vector>
#include <algorithm>


int main(const int argc, const char *const *const argv)
{
	int n, t;
	std::cin >> n >> t;
	std::vector<int> as(n);
	for (int i = 0; i < n; ++i) {
		std::cin >> as[i];
	}

	int time = 0;
	int score = 0;
	bool judge = true;
	for (const auto a : as) {
		time += t;
		const bool currentJudge = !!(time <= a);
		if (!judge && !currentJudge) {
			break;
		}
		time = std::max(time, a);
		score += currentJudge;
		judge = currentJudge;
	}
	std::cout << score << std::endl;

	return 0;
}
