﻿#include <iostream>


int main()
{
    size_t n;
    std::cin >> n;
    for (auto i = size_t(0); i < n; ++i) {
        int k, p;
        std::cin >> k >> p;
        std::cout << (k % p == 0 ? p : k % p) << std::endl;
    }

    return 0;
}
