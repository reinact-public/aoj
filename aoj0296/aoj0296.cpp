﻿#include <iostream>


int main()
{
	int c1, c5, c10, c50, c100, c500;
	std::cin >> c1 >> c5 >> c10 >> c50 >> c100 >> c500;
	std::cout << !!(c1 + 5 * c5 + 10 * c10 + 50 * c50 + 100 * c100 + 500 * c500 >= 1000) << std::endl;

	return 0;
}
