﻿#include <iostream>


constexpr int COSTS[] = { 6000, 4000, 3000, 2000 };


constexpr int solve(const int t, const int n) {
	return COSTS[t - 1] * n;
}


int main() {
	for (int i = 0; i < 4; ++i) {
		int t, n;
		std::cin >> t >> n;
		std::cout << solve(t, n) << std::endl;
	}

	return 0;
}
