﻿#include <algorithm>
#include <array>
#include <iostream>
#include <stack>
#include <string>
#include <vector>


namespace {
    const std::array<int, 12> digits{
        1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049, 177147 };
    std::vector<std::string> opt(100001);
}


int main()
{
    std::stack<std::pair<std::string, int>> queue;
    queue.emplace("", 0);
    while (!queue.empty()) {
        const auto e = std::move(queue.top());
        queue.pop();
        if ((e.first.empty() || e.first[0] != '0') && 0 <= e.second && e.second < opt.size()) {
            opt[e.second] = e.first;
        }
        if (e.first.length() < digits.size()) {
            queue.emplace(std::move("+" + e.first), e.second + digits[e.first.length()]);
            queue.emplace(std::move("-" + e.first), e.second - digits[e.first.length()]);
            queue.emplace(std::move("0" + e.first), e.second);
        }
    }
    
    int w;
    std::cin >> w;
    std::cout << opt[w] << std::endl;

    return 0;
}
